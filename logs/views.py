from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from models import *
from grab import Grab
import httplib, urllib, base64
import logging
import json
import re
import string
import random
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from googleapiclient.discovery import build
from django.http import HttpResponseNotFound

# Create your views here.
logger = logging.getLogger(__name__)


def index(request):
    current_site = request.META['HTTP_HOST']
    # get_current_site(request)
    try:
        domain = Domain.objects.get(name=current_site)
    except Domain.DoesNotExist:
        domain = Domain()
        domain.name = current_site
        domain.keyword = Keyword.objects.random()
        domain.save()
    context = {}
    keyword = domain.keyword.name.capitalize()
    second_keyword = Keyword.objects.random().name.capitalize()
    try:
        page = Page.objects.get(domain=domain, keyword=domain.keyword)
    except Page.DoesNotExist:
        page = Page()
        page.domain = domain
        page.keyword = domain.keyword
        page.second_keyword = second_keyword
        page.title = keyword.capitalize() + ' | ' + second_keyword.capitalize()
        images = Image.objects.filter(keyword=page.keyword)
        if not images:
            images = []
            g = Grab()
            params = urllib.urlencode({
                # Request parameters
                'q': keyword,
                'count': '50',
                'offset': '0',
                'mkt': 'en-us',
                'safeSearch': 'Moderate',
            })
            headers = {
                # Request headers
                'Ocp-Apim-Subscription-Key': 'd057d23febc24e33b31063948ddcdecf',
            }
            g.setup(headers=headers)
            try:
                g.go('https://api.cognitive.microsoft.com/bing/v5.0/images/search?%s' % params)
            except Exception as e:
                logger.error("[Errno {0}] {1}".format(e.errno, e.strerror))
            bing_images = json.loads(g.response.body)
            for bing_image in bing_images['value']:
                image = Image()
                regex = re.compile('[%s]' % re.escape(string.punctuation))
                image.name = regex.sub('', bing_image['name'])
                image.keyword = page.keyword
                image.datePublished = bing_image['datePublished']
                image_url = re.search(r'r=(.*?)&', bing_image['contentUrl'])
                image_clean_url = urllib.unquote(image_url.group(1)).decode('utf8')
                image.contentUrl = image_clean_url
                image.contentSize = bing_image['contentSize']
                image.encodingFormat = bing_image['encodingFormat']
                image.hostPageDisplayUrl = bing_image['hostPageDisplayUrl'].split('/')[0]
                image.width = bing_image['width']
                image.height = bing_image['height']
                image.save()
                images.append(image)
        page.description = keyword
        for i in xrange(0, randint(8, 11)):
            page.description = page.description + ' ' + Keyword.objects.random().name
        page.description = page.description + ' ' + keyword
        page.keywords = keyword
        for i in xrange(0, randint(8, 11)):
            page.keywords = page.keywords + ', ' + Keyword.objects.random().name
        content = []
        for i in xrange(0, randint(2, 5)):
            content_block = {}
            rand_image = random.choice(images)
            content_block['name'] = rand_image.name
            content_block['content_url'] = rand_image.contentUrl.replace('http://', '')
            content_block['host'] = rand_image.hostPageDisplayUrl
            content_block['width'] = rand_image.width
            content_block['height'] = rand_image.height
            content_block['size'] = rand_image.contentSize
            content_block['format'] = rand_image.encodingFormat
            content_block['published'] = rand_image.datePublished
            content_block['text'] = rand_image.name
            for i in xrange(0, randint(5, 8)):
                content_block['text'] = content_block['text'] + ' ' + random.choice(images).name.lower()
            content.append(content_block)
        page.set_content(content)
        page.post_id = randint(1000, 100000)
        g = Grab()
        g.go('https://randomuser.me/api/')
        rand_user_get = g.response.body
        rand_user_name = json.loads(rand_user_get)['results'][0]['name']['first']
        plus_user = get_plus_user(rand_user_name)['items'][0]['id']
        page.plus_id = plus_user
        page.save()
    rand_links = []
    # for i in xrange(0, randint(200, 300)):
    #    rand_key = Keyword.objects.random().name
    #    item = {'url': 'http://' + domain.name + '/' + rand_key.replace(' ', '-') + '.html',
    #            'keyword': rand_key}
    #    rand_links.append(item)
    keywords = Keyword.objects.order_by('?')[0:randint(200, 300)]
    for keyw in keywords:
        kewrd = keyw.name.capitalize()
        item = {'url': 'http://' + domain.name + '/' + kewrd.replace(' ', '-') + '.html', 'keyword': kewrd}
        rand_links.append(item)
    context['content'] = page.get_content()
    context['keyword'] = page.keyword.name.capitalize()
    context['second_keyword'] = page.second_keyword
    context['keywords'] = page.keywords
    context['description'] = page.description
    context['post_id'] = page.post_id
    context['plus_id'] = page.plus_id
    context['title'] = page.title
    context['rand_links'] = rand_links
    context['domain'] = domain.name
    context['permalink'] = request.build_absolute_uri()
    return render(request, 'theme.html', context)


def page(request, keyword):
    keyword = keyword.replace('-', ' ').lower()
    try:
        key_obj = Keyword.objects.get(name=keyword)
    except Keyword.DoesNotExist:
        return HttpResponseNotFound('<h1>Page not found</h1>')
    current_site = request.META['HTTP_HOST']
    try:
        domain = Domain.objects.get(name=current_site)
    except Domain.DoesNotExist:
        domain = Domain()
        domain.name = current_site
        domain.keyword = Keyword.objects.random()
        domain.save()
    context = {}
    # keyword = domain.keyword.name.capitalize()
    second_keyword = Keyword.objects.random().name.capitalize()
    try:
        page = Page.objects.get(domain=domain, keyword=key_obj)
    except Page.DoesNotExist:
        page = Page()
        page.domain = domain
        page.keyword = key_obj
        page.second_keyword = second_keyword
        page.title = keyword.capitalize() + ' | ' + second_keyword.capitalize()
        images = Image.objects.filter(keyword=key_obj)
        if not images:
            images = []
            g = Grab()
            params = urllib.urlencode({
                # Request parameters
                'q': keyword,
                'count': '50',
                'offset': '0',
                'mkt': 'en-us',
                'safeSearch': 'Moderate',
            })
            headers = {
                # Request headers
                'Ocp-Apim-Subscription-Key': 'd057d23febc24e33b31063948ddcdecf',
            }
            g.setup(headers=headers)
            try:
                g.go('https://api.cognitive.microsoft.com/bing/v5.0/images/search?%s' % params)
            except Exception as e:
                logger.error("[Errno {0}] {1}".format(e.errno, e.strerror))
            bing_images = json.loads(g.response.body)
            for bing_image in bing_images['value']:
                image = Image()
                regex = re.compile('[%s]' % re.escape(string.punctuation))
                image.name = regex.sub('', bing_image['name'])
                image.keyword = key_obj
                image.datePublished = bing_image['datePublished']
                image_url = re.search(r'r=(.*?)&', bing_image['contentUrl'])
                image_clean_url = urllib.unquote(image_url.group(1)).decode('utf8')
                image.contentUrl = image_clean_url
                image.contentSize = bing_image['contentSize']
                image.encodingFormat = bing_image['encodingFormat']
                image.hostPageDisplayUrl = bing_image['hostPageDisplayUrl'].split('/')[0]
                image.width = bing_image['width']
                image.height = bing_image['height']
                image.save()
                images.append(image)
        page.description = keyword
        for i in xrange(0, randint(8, 11)):
            page.description = page.description + ' ' + Keyword.objects.random().name
        page.description = page.description + ' ' + keyword
        page.keywords = keyword
        for i in xrange(0, randint(8, 11)):
            page.keywords = page.keywords + ', ' + Keyword.objects.random().name
        content = []
        for i in xrange(0, randint(2, 5)):
            content_block = {}
            rand_image = random.choice(images)
            content_block['name'] = rand_image.name
            content_block['content_url'] = rand_image.contentUrl.replace('http://', '')
            content_block['host'] = rand_image.hostPageDisplayUrl
            content_block['width'] = rand_image.width
            content_block['height'] = rand_image.height
            content_block['size'] = rand_image.contentSize
            content_block['format'] = rand_image.encodingFormat
            content_block['published'] = rand_image.datePublished
            content_block['text'] = rand_image.name
            for i in xrange(0, randint(5, 8)):
                content_block['text'] = content_block['text'] + ' ' + random.choice(images).name.lower()
            content.append(content_block)
        page.set_content(content)
        page.post_id = randint(1000, 100000)
        g = Grab()
        g.go('https://randomuser.me/api/')
        rand_user_get = g.response.body
        rand_user_name = json.loads(rand_user_get)['results'][0]['name']['first']
        plus_user = get_plus_user(rand_user_name)['items'][0]['id']
        page.plus_id = plus_user
        page.save()
    rand_links = []
    # for i in xrange(0, randint(200, 300)):
    #    rand_key = Keyword.objects.random().name
    #    item = {'url': 'http://' + domain.name + '/' + rand_key.replace(' ', '-') + '.html',
    #            'keyword': rand_key}
    #    rand_links.append(item)
    keywords = Keyword.objects.order_by('?')[0:randint(200, 300)]
    for keyw in keywords:
        kewrd = keyw.name.capitalize()
        item = {'url': 'http://' + domain.name + '/' + kewrd.replace(' ', '-') + '.html', 'keyword': kewrd}
        rand_links.append(item)
    context['content'] = page.get_content()
    context['keyword'] = page.keyword.name.capitalize()
    context['second_keyword'] = page.second_keyword
    context['keywords'] = page.keywords
    context['description'] = page.description
    context['post_id'] = page.post_id
    context['plus_id'] = page.plus_id
    context['title'] = page.title
    context['rand_links'] = rand_links
    context['domain'] = domain.name
    context['permalink'] = request.build_absolute_uri()
    return render(request, 'theme.html', context)


def get_plus_user(name):
    scopes = ['https://www.googleapis.com/auth/plus.login']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        './static/plus.json', scopes=scopes)
    http_auth = credentials.authorize(Http())
    plus_api = build('plus', 'v1', http=http_auth)
    response = plus_api.people().search(maxResults=1, query=name).execute()
    return response
