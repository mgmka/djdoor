from django.shortcuts import render
from models import *
from grab import Grab
import logging
import json
import re
import string
import random
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from googleapiclient.discovery import build
from django.http import HttpResponseNotFound
from datetime import datetime, timedelta
from django.http import HttpResponse

logger = logging.getLogger(__name__)


def index(request):
    current_site = request.META['HTTP_HOST']
    try:
        domain = Domain.objects.get(name=current_site)
    except Domain.DoesNotExist:
        domain = Domain()
        domain.name = current_site
        domain.keyword = Keyword.objects.random()
        #g = Grab()
        #g.go('https://randomuser.me/api/')
        #rand_user_get = g.response.body
        #rand_user_name = json.loads(rand_user_get)['results'][0]['name']['first']
        plus_user = get_plus_user(random.choice(string.letters))['items'][0]['id']
        domain.plus_id = plus_user
        domain.map_count = randint(5000, 10000)
        domain.save()
    context = {}
    keyword = domain.keyword.name.capitalize()
    second_keyword = Keyword.objects.random().name.capitalize()
    try:
        page = Page.objects.get(domain=domain, keyword=domain.keyword)
    except Page.DoesNotExist:
        page = Page()
        page.domain = domain
        page.keyword = domain.keyword
        page.second_keyword = second_keyword
        page.title = keyword.capitalize() + ' | ' + second_keyword.capitalize()
        images = Image.objects.filter(keyword=page.keyword)
        if not images:
            images = []
            g = Grab()
            try:
                g.go('http://www.bing.com/images/async?q=%s&async=content&first=1&count=100' % keyword)
            except Exception as e:
                logger.error("[Errno {0}] {1}".format(e.errno, e.strerror))
            response = g.response.body
            names = re.findall(r't1="(.*?)"', response)
            infos = re.findall(r't2="(.*?)"', response)
            host_urls = re.findall(r't3="(.*?)"', response)
            image_urls = re.findall(r'imgurl:&quot;(.*?)&quot;', response)
            for name, info, host_url, image_url in zip(names, infos, host_urls, image_urls):
                image = Image()
                regex = re.compile('[%s]' % re.escape(string.punctuation))
                image.name = regex.sub('', name)
                image.keyword = page.keyword
                info_list = info.split()
                image.datePublished = (datetime.today() - timedelta(days=random.randint(0, 10))).strftime(
                    "%Y-%m-%d %H:%M:%S")
                image.contentUrl = image_url
                image.contentSize = info_list[4] + info_list[5]
                image.encodingFormat = info_list[7]
                image.hostPageDisplayUrl = host_url.split('/')[0]
                image.width = info_list[0]
                image.height = info_list[2]
                image.save()
                images.append(image)
        page.description = keyword
        for i in xrange(0, randint(8, 11)):
            page.description = page.description + ' ' + Keyword.objects.random().name
        page.description = page.description + ' ' + keyword
        page.keywords = keyword
        for i in xrange(0, randint(8, 11)):
            page.keywords = page.keywords + ', ' + Keyword.objects.random().name
        content = []
        for i in xrange(0, randint(2, 5)):
            content_block = {}
            rand_image = random.choice(images)
            content_block['name'] = rand_image.name
            content_block['content_url'] = rand_image.contentUrl.replace('http://', '')
            content_block['host'] = rand_image.hostPageDisplayUrl
            content_block['width'] = rand_image.width
            content_block['height'] = rand_image.height
            content_block['size'] = rand_image.contentSize
            content_block['format'] = rand_image.encodingFormat
            content_block['published'] = rand_image.datePublished
            content_block['text'] = rand_image.name
            maxi = 8
            if len(images) < 8:
                maxi = len(images)
            for i in xrange(0, randint(maxi//2, maxi)):
                content_block['text'] = content_block['text'] + ' ' + random.choice(images).name.lower()
            content.append(content_block)
        page.set_content(content)
        page.post_id = randint(1000, 100000)
        page.save()
    rand_links = []
    keywords = Keyword.objects.order_by('?')[0:randint(50, 100)]
    for keyw in keywords:
        kewrd = keyw.name.capitalize()
        item = {'url': 'http://' + domain.name + '/' + kewrd.replace(' ', '-') + '.html', 'keyword': kewrd}
        rand_links.append(item)
    context['content'] = page.get_content()
    context['keyword'] = page.keyword.name.capitalize()
    context['second_keyword'] = page.second_keyword
    context['keywords'] = page.keywords
    context['description'] = page.description
    context['post_id'] = page.post_id
    context['plus_id'] = domain.plus_id
    context['title'] = page.title
    context['rand_links'] = rand_links
    context['domain'] = domain.name
    context['permalink'] = request.build_absolute_uri()
    user_agent = request.META.get('HTTP_USER_AGENT', None)
    referer = request.META.get('HTTP_REFERER', '')
    script = False
    if 'google' in referer or 'bing' in referer or 'yahoo' in referer:
        script = True
    if user_agent:
        if 'google' in user_agent.lower():
            script = False
    context['script'] = script
    return render(request, 'theme.html', context)


def page(request, keyword):
    keyword = keyword.replace('-', ' ').lower()
    try:
        key_obj = Keyword.objects.get(name=keyword)
    except Keyword.DoesNotExist:
        return HttpResponseNotFound('<h1>Page not found</h1>')
    current_site = request.META['HTTP_HOST']
    try:
        domain = Domain.objects.get(name=current_site)
    except Domain.DoesNotExist:
        domain = Domain()
        domain.name = current_site
        domain.keyword = Keyword.objects.random()
        #g = Grab()
        #g.go('https://randomuser.me/api/')
        #rand_user_get = g.response.body
        #rand_user_name = json.loads(rand_user_get)['results'][0]['name']['first']
        plus_user = get_plus_user(random.choice(string.letters))['items'][0]['id']
        domain.plus_id = plus_user
        domain.map_count = randint(5000, 10000)
        domain.save()
    context = {}
    second_keyword = Keyword.objects.random().name.capitalize()
    try:
        page = Page.objects.get(domain=domain, keyword=key_obj)
    except Page.DoesNotExist:
        page = Page()
        page.domain = domain
        page.keyword = key_obj
        page.second_keyword = second_keyword
        page.title = keyword.capitalize() + ' | ' + second_keyword.capitalize()
        images = Image.objects.filter(keyword=key_obj)
        if not images:
            images = []
            g = Grab()
            try:
                g.go('http://www.bing.com/images/async?q=%s&async=content&first=1&count=100' % keyword)
            except Exception as e:
                logger.error("[Errno {0}] {1}".format(e.errno, e.strerror))
            response = g.response.body
            names = re.findall(r't1="(.*?)"', response)
            infos = re.findall(r't2="(.*?)"', response)
            host_urls = re.findall(r't3="(.*?)"', response)
            image_urls = re.findall(r'imgurl:&quot;(.*?)&quot;', response)
            for name, info, host_url, image_url in zip(names, infos, host_urls, image_urls):
                image = Image()
                regex = re.compile('[%s]' % re.escape(string.punctuation))
                image.name = regex.sub('', name)
                image.keyword = page.keyword
                info_list = info.split()
                image.datePublished = (datetime.today() - timedelta(days=random.randint(0, 10))).strftime(
                    "%Y-%m-%d %H:%M:%S")
                image.contentUrl = image_url
                image.contentSize = info_list[4] + info_list[5]
                image.encodingFormat = info_list[7]
                image.hostPageDisplayUrl = host_url.split('/')[0]
                image.width = info_list[0]
                image.height = info_list[2]
                image.save()
                images.append(image)
        page.description = keyword
        for i in xrange(0, randint(8, 11)):
            page.description = page.description + ' ' + Keyword.objects.random().name
        page.description = page.description + ' ' + keyword
        page.keywords = keyword
        for i in xrange(0, randint(8, 11)):
            page.keywords = page.keywords + ', ' + Keyword.objects.random().name
        content = []
        for i in xrange(0, randint(2, 5)):
            content_block = {}
            rand_image = random.choice(images)
            content_block['name'] = rand_image.name
            content_block['content_url'] = rand_image.contentUrl.replace('http://', '')
            content_block['host'] = rand_image.hostPageDisplayUrl
            content_block['width'] = rand_image.width
            content_block['height'] = rand_image.height
            content_block['size'] = rand_image.contentSize
            content_block['format'] = rand_image.encodingFormat
            content_block['published'] = rand_image.datePublished
            content_block['text'] = rand_image.name
            maxi = 8
            if len(images) < 8:
                maxi = len(images)
            for i in xrange(0, randint(maxi // 2, maxi)):
                content_block['text'] = content_block['text'] + ' ' + random.choice(images).name.lower()
            content.append(content_block)
        page.set_content(content)
        page.post_id = randint(1000, 100000)
        page.save()
    rand_links = []
    count_keys = randint(50, 100)
    count_all = Keyword.objects.count()
    start = randint(0, count_all - count_keys - 1)
    keywords = Keyword.objects.all()[start:start + count_keys]
    for keyw in keywords:
        kewrd = keyw.name.capitalize()
        item = {'url': 'http://' + domain.name + '/' + kewrd.replace(' ', '-') + '.html', 'keyword': kewrd}
        rand_links.append(item)
    context['content'] = page.get_content()
    context['keyword'] = page.keyword.name.capitalize()
    context['second_keyword'] = page.second_keyword
    context['keywords'] = page.keywords
    context['description'] = page.description
    context['post_id'] = page.post_id
    context['plus_id'] = domain.plus_id
    context['title'] = page.title
    context['rand_links'] = rand_links
    context['domain'] = domain.name
    context['permalink'] = request.build_absolute_uri()
    user_agent = request.META.get('HTTP_USER_AGENT', None)
    referer = request.META.get('HTTP_REFERER', '')
    script = False
    if 'google' in referer or 'bing' in referer or 'yahoo' in referer:
        script = True
    if user_agent:
        if 'google' in user_agent.lower():
            script = False
    context['script'] = script
    return render(request, 'theme.html', context)


def robots(request):
    current_site = request.META['HTTP_HOST']
    out = 'Sitemap: http://' + current_site + '/sitemap.xml'
    return HttpResponse(out)


def sitemap(request):
    current_site = request.META['HTTP_HOST']
    try:
        domain = Domain.objects.get(name=current_site)
        map_count = domain.map_count
    except Domain.DoesNotExist:
        HttpResponseNotFound('<h1>Page not found</h1>')
    count_all = Keyword.objects.count()
    count_maps = count_all // map_count
    result = '<?xml version="1.0" encoding="UTF-8"?>\r\n'
    result += '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\r\n'
    for i in range(0, count_maps):
        result += '<sitemap>\r\n'
        result += ' <loc>http://' + current_site + '/sitemap' + str(i) + '.xml</loc>\r\n'
        result += '</sitemap>\r\n'
    result += '</sitemapindex>\r\n'
    return HttpResponse(result, content_type='text/xml')


def sitemap_page(request, num):
    current_site = request.META['HTTP_HOST']
    try:
        domain = Domain.objects.get(name=current_site)
        map_count = domain.map_count
    except Domain.DoesNotExist:
        HttpResponseNotFound('<h1>Page not found</h1>')
    # count_all = Keyword.objects.count()
    links = []
    keys = Keyword.objects.all()[int(num) * map_count:(int(num) + 1) * map_count]
    for key in keys:
        keyword = key.name.capitalize()
        link = 'http://' + current_site + '/' + keyword.replace(' ', '-') + '.html'
        links.append(link)
    context = {}
    context['urls'] = links
    return render(request, 'sitemap.html', context, content_type='text/xml')


def feed(request):
    return HttpResponse('')


def get_plus_user(name):
    scopes = ['https://www.googleapis.com/auth/plus.login']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        '/home/mgm/PycharmProjects/djdoor/static/plus.json', scopes=scopes)
    http_auth = credentials.authorize(Http())
    plus_api = build('plus', 'v1', http=http_auth)
    response = plus_api.people().search(maxResults=1, query=name).execute()
    return response


def plus_id():
    result = '10'
    for i in range(0, 19):
        result += str(randint(0, 9))
    return result


def add_verification(request, keyword):
    verification = Verification()
    verification.google = keyword
    verification.save()
    return HttpResponse('ok')


def google(request, keyword):
    google_key = Verification.objects.filter(google=keyword).first()
    if google_key:
        return HttpResponse('google-site-verification: google' + google_key.google + '.html')
    return HttpResponseNotFound('<h1>Page not found</h1>')
