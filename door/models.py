from __future__ import unicode_literals

from django.db import models
import json
from django.db.models.aggregates import Count
from random import randint


# Create your models here.


class KeywordManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]


class Keyword(models.Model):
    name = models.CharField(max_length=500)
    objects = KeywordManager()


class Domain(models.Model):
    name = models.CharField(max_length=100)
    keyword = models.ForeignKey(Keyword)
    plus_id = models.CharField(max_length=50)
    map_count = models.IntegerField()


class Image(models.Model):
    name = models.CharField(max_length=500)
    datePublished = models.CharField(max_length=20)
    contentUrl = models.CharField(max_length=500)
    contentSize = models.CharField(max_length=10)
    encodingFormat = models.CharField(max_length=5)
    hostPageDisplayUrl = models.CharField(max_length=500)
    width = models.CharField(max_length=5)
    height = models.CharField(max_length=5)
    keyword = models.ForeignKey(Keyword)


class Page(models.Model):
    domain = models.ForeignKey(Domain)
    keyword = models.ForeignKey(Keyword)
    second_keyword = models.CharField(max_length=500)
    title = models.CharField(max_length=500)
    description = models.CharField(max_length=700)
    keywords = models.CharField(max_length=700)
    content = models.TextField()
    post_id = models.IntegerField()

    # plus_id = models.CharField(max_length=50)
    # copyrighting = models.CharField(max_length=500)
    # links = models.TextField()

    def set_content(self, x):
        self.content = json.dumps(x)

    def get_content(self):
        return json.loads(self.content)

        # def set_links(self, x):
        # self.links = json.dumps(x)

        # def get_links(self):
        # return json.loads(self.links)


class Verification(models.Model):
    google = models.CharField(max_length=20)
