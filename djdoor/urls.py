"""djdoor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from door.views import index, page, google, add_verification, robots, sitemap, feed, sitemap_page

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^robots.txt', robots),
    url(r'^sitemap.xml', sitemap),
    url(r'^sitemap(?P<num>[0-9]+).xml', sitemap_page),
    url(r'^feed', feed),
    url(r'^add/(?P<keyword>.*)$', add_verification),
    url(r'^google(?P<keyword>.*)\.html$', google),
    url(r'^(?P<keyword>.*)\.html$', page)
]
